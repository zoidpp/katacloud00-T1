FROM node:7.7.4-alpine

RUN git clone https://github.com/vuejs/vue-hackernews-2.0.git app
COPY app/build /usr/app
WORKDIR /usr/app

RUN npm install -g serve

EXPOSE 8080
CMD [ "serve", "-s /usr/app" ]
